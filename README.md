# GNUstep Platinum Theme

Inspired by Rhapsody, here is an attempt to port the Platinum theme from Classic Mac OS to GNUstep.

![screenshot](screenshot.jpg)

For personal/entertainment use only.
Rhapsody, NeXT and Apple related trademarks belong to Apple.

## Installation:

Clone this directory into your Library/Themes directory.

On most GNUstep setups that'd probably be in ~/GNUstep/Library/Themes.

Then you can issue `defaults write NSGlobalDomain GSTheme platinum` if your cloned directory is named `platinum.theme` (as is the name of this git repository)

## Issues

While what you see is what you get, there's several inconsistencies.
For those you'd have to use GSTheme's framework of overriding rendering functionality, perhaps. Or there's some tweaks in the .plist we can do to fix them.

Examples:

1. First of all, the fonts are all wrong. Those we can change, I just didn't have time to rip them. Should be trivial. You can override the various fonts used within the .plist under ThemeDomain.
2. The background of some control elements isn't the same shade of white as the control elements themselves. You can see that in e.g. the font picker under 'Typeface'. This should be trivial but I haven't had the time to figure out which field in ThemeColors.clr changes it.
3. Only the bare minimum of icons have been changed to Rhapsody. Still uses NeXT stuff all over otherwise.
4. The focus indicator looks good only on some elements. We'd have to force it to always be external - I am not sure. I need an actual Rhapsody machine or VM to look at it.
5. Borders. Things like scrollbars have the wrong borders. I am not sure what the best way to proceed there, but when in doubt one can override it using GSTheme code override.
6. Probably lots of small details I haven't caught when copying screenshots from [GUIdebook](https://guidebookgallery.org/screenshots/rhapsodydr2)